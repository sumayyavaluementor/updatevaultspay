#!/bin/bash
# Set up Laravel after main deployment. Called from CodeDeploy's
# appspec.yml.
PARAMATER="APP_ENV"
REGION="me-south-1"
WEB_DIR="/data/vaultspay/"
WEB_USER="ec2-user"
WEB_GROUP="nginx"
chmod -R 775 /data/vaultspay
find /data -type f -exec chmod 0644 {} \;
# Install project dependencies
cd /data/vaultspay
rm -rf .env
#composer install
php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
sudo php composer-setup.php --quiet --install-dir=/usr/local/bin/ --filename=composer
rm -f composer-setup.php
export COMPOSER_HOME="$HOME/.config/composer";
composer install
# Get parameters and put it into .env file inside application root
aws ssm get-parameter --with-decryption --name $PARAMATER --region $REGION --query Parameter.Value | sed -e 's/^"//' -e 's/"$//' -e 's/\\n/\n/g' -e 's/\\//g' > $WEB_DIR/.env
sudo usermod -a -G nginx ec2-user
chown -R $WEB_GROUP:$WEB_GROUP /data/vaultspay
php artisan config:clear

